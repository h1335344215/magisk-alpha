## 0bb64ac4-德尔塔

- 如果已经安装，安装镜子不会再次安装

Canary 和 Debug 是从相同的源代码构建的。调试版本有更详细的日志，适合调试。Canary 构建日志更少，比 Debug 更稳定，并且适合大多数常见用途

如果你喜欢我的作品，你可以通过 [PayPal/HuskyDG](http://paypal.me/huskydg) 捐赠我

### 与官方 Magisk 不同

- [常规] 使用“卸载模块”按模块管理修改文件的可见性
- [管理器] 支持将模拟器安装到系统分区
- [常规] 将“addon.d”所需的文件复制到“/system”
- [管理器] 在中文 ROM 的语言设置中显示所有支持的语言
- [模块]支持无系统删除模块的文件或文件夹
- [常规] 内置引导循环保护，通过 Magisk 模块保护系统免受引导循环影响
- [常规] 调整 F2FS 驱动程序以修复未加密的 f2fs `/data` 上的问题
- [MagiskInit] 支持 Pre-Init 挂载，在 `init` 启动之前替换系统文件
- [MagiskInit] 支持从 pre-init 目录加载自定义 rc 脚本
- [模块] 支持魔术挂载更多分区（`my_*`、`odm`、`optics`、`prism`）
- [Zygisk]：通过 5ec1cff 切换到使用本机桥
- [模块] 实时补丁“sepolicy.rule”（如果在“sepolicy.rules”目录中找不到）
- [MagiskSU] 不要使用本地套接字路径
- [模块] 始终注入 Magisk bin
- [MagiskInit] 引入 `early-mount.d/v2` 以支持每个模块的预初始化安装

### Magisk 上游

- 将上游源代码同步到350d0d600
